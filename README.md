#NMED 3720 - Assignment 3

#In this assignment my goal was to create an interface in which fans of the TV show Rupaul’s Drag Race would “find out which queen they are” inspired by Buzzfeed quizzes that have a similar premise. Unlike in Buzzfeed quizzes, they do not have to answer any series of questions and all seasons are in one spot. The site will simply generate a drag queen based on the click of a button. First, users will select a season of their choosing, maybe their favourite season or possibly a lucky number if they are not as familiar with the show. Then, they are randomly given a drag queen that would represent who they are. Information given includes the drag queens twitter profile picture, their twitter name, and their description/bio. I felt that these pieces of information made most sense to include because it gives a small insight to who the drag queens are without venturing farther than the twitter API. Users are then able to select another season if they are not happy with their results. Not all drag queens from the show are represented unfortunately. I selected drag queens based on a few factors: how memorable/popular I believe they were, how long they lasted on the show, how big their personality is and if they had an active twitter profile. 

#all information is taken directly from each drag queens twitter profile
#help and code from https://www.w3schools.com/css/css3_gradients.asp, https://www.w3schools.com/js/js_errors.asp, https://dmitripavlutin.com/7-tips-to-handle-undefined-in-javascript/, https://dzone.com/articles/typeerror-javascript, https://www.w3schools.com/jsref/jsref_replace.asp, https://developer.twitter.com/en/docs.html
#Designed by Alexandra Thompson 2019

#To use, click on any of the numbered buttons representing seasons, click as many times as you like. if you click and nothing happens just try again it probably generated the same user twice in a row.

